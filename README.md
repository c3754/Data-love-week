# RDC rundown DLI 2022

Purpose:
Repository of materials delivered during love data week 2023


Contents:

- Acronym_list.xlsx - List of acronyms & initialisms used in the presentation
- RDC_rundown_no_video_.pptx - Slides with no multimedia content
- RDC_rundown_with_video_.pptx - Slides with video/audio content as used in the presentation
- license - CCBYNC licensing agreement
